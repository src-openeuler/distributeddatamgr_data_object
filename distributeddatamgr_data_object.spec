%define debug_package %{nil}
%global oh_version OpenHarmony-v3.2-Release
%global distributeddata_dir %{_builddir}/foundation/distributeddatamgr/
%global build_opt /opt/distributed-middleware-build
%global bundle_dir %{build_opt}/openeuler/compiler_gn/foundation/distributeddatamgr/data_object

Name:       distributeddatamgr_data_object
Version:    1.0.0
Release:    3
Summary:    Distributed Data Object Management Framework
License:    Apache-2.0
Url:        https://gitee.com/openharmony/distributeddatamgr_data_object
Source1:    https://gitee.com/openharmony/distributeddatamgr_data_object/repository/archive/OpenHarmony-v3.2-Release.tar.gz#/distributeddatamgr_data_object-OpenHarmony-v3.2-Release.tar.gz
Source2:    distributeddataobject_impl.BUILD.gn
Source3:    bundle.json
Patch1:     0001-Trim-the-dependencies-on-hitrace-libuv.patch

BuildRequires: libboundscheck
BuildRequires: distributed-build, hilog, commonlibrary_c_utils
BuildRequires: communication_ipc, communication_dsoftbus
BuildRequires: systemabilitymgr_safwk, systemabilitymgr_samgr
BuildRequires: distributedhardware_device_manager
BuildRequires: distributeddatamgr_kv_store

Requires: libboundscheck
Requires: hilog, commonlibrary_c_utils
Requires: communication_ipc, communication_dsoftbus
Requires: systemabilitymgr_safwk, systemabilitymgr_samgr
Requires: distributedhardware_device_manager
Requires: distributeddatamgr_kv_store, distributeddatamgr_datamgr_service

%description
The object-oriented memory data management framework provides application 
developers with basic data object management capabilities such as creating, 
querying, deleting, modifying, and subscribing to memory objects. It also 
has distributed capabilities to meet the collaborative needs of data objects 
between multiple devices in the same application in the super terminal scenario.
Developers can easily use distributed object interfaces for object synchronization within trusted devices.

%prep
rm -rf %{_builddir}/*
cp -rf %{build_opt} %{_builddir}/build
[ ! -L "%{_builddir}/build.sh" ] && ln -s %{_builddir}/build/build_scripts/build.sh %{_builddir}/build.sh
[ ! -L "%{_builddir}/.gn" ] && ln -s %{_builddir}/build/core/gn/dotfile.gn %{_builddir}/.gn
[ ! -L "%{_builddir}/build.py" ] && ln -s %{_builddir}/build/lite/build.py %{_builddir}/build.py
cp -rf %{_builddir}/build/openeuler/vendor %{_builddir}/
cp -rf %{_builddir}/build/openeuler/compiler_gn/* %{_builddir}

%setup -q -D -T -a 1 -c -n %{distributeddata_dir}
if [ -d "%{distributeddata_dir}/data_object" ]; then
  rm -rf %{distributeddata_dir}/data_object
fi
mv %{distributeddata_dir}/%{name}-%{oh_version} %{distributeddata_dir}/data_object

%patch -P1 -p1 -d %{distributeddata_dir}data_object

%build
%ifarch x86_64
%{_builddir}/build.sh --product-name openeuler --target-cpu x86_64
%endif

%ifarch aarch64
%{_builddir}/build.sh --product-name openeuler --target-cpu arm64
%endif

%install
install -d -m 0755 %{buildroot}/%{_includedir}/data_object
install -d -m 0755 %{buildroot}/%{_libdir}
install -d -m 0755 %{buildroot}/%{bundle_dir}/interfaces/innerkits
install -d -m 0755 %{buildroot}/system/lib64

cp %{SOURCE2} %{buildroot}/%{bundle_dir}/interfaces/innerkits/BUILD.gn
cp %{SOURCE3} %{buildroot}/%{bundle_dir}/

%ifarch aarch64
%define lib_out_path out/openeuler/linux_clang_arm64/distributeddatamgr/data_object
%define header_out_path out/openeuler/innerkits/linux-arm64/data_object
%endif
%ifarch x86_64
%define lib_out_path out/openeuler/linux_clang_x86_64/distributeddatamgr/data_object
%define header_out_path out/openeuler/innerkits/linux-x86_64/data_object
%endif

install -m 0755 %{_builddir}/%{lib_out_path}/*.so %{buildroot}/%{_libdir}
install -m 0755 %{_builddir}/%{lib_out_path}/*.so %{buildroot}/system/lib64

find %{_builddir}/%{header_out_path} -name *.h -print0 | xargs -0 -i cp -rf {} %{buildroot}%{_includedir}/data_object/

pushd %{_builddir}/foundation/distributeddatamgr/data_object
for include_file in `find . \( -name test -o -name jskitsimpl \) -prune -o -name "*.h" -type f`
do
  if [[ ${include_file} != *.h ]]; then
    continue
  fi
  cp --parents ${include_file} %{buildroot}%{bundle_dir}
done
popd

%files
%{_libdir}/*.so
%{_includedir}/data_object/*
%{bundle_dir}/*
/system/*

%changelog
* Tue Nov 21 2023 Peng He <hepeng68@huawei.com> - 1.0.0-3
- Update build requires and requires.

* Thu Aug 03 2023 Peng He <hepeng68@huawei.com> - 1.0.0-2
- Add requires and rectify the contents of the RPM package.

* Wed Jun 07 2023 Ge Wang <wang__ge@126.com> - 1.0.0-1
- init package
